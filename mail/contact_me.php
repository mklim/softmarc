<?php
// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "No arguments Provided!";
	return false;
   }
	
$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];
	
$headers   = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/plain; charset=UTF-8";
$headers[] = "From: ".$email_address;
// Create the email and send the message
$to = 'softmarcpoland@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
$email_subject = "Formularz kontaktowy - softmarc.pl";
$email_body = "Formularz kontaktowy:\n\nName: $name\n\nEmail: $email_address\n\nWiadomość:\n$message";
//$headers = "Formularz kontaktowy - softmarc.pl"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
mail($to,$email_subject,$email_body,implode("\r\n", $headers));
return true;			
?>
